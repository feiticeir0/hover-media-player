#!/usr/bin/env python
""" Play music files using the pygame mixer module and hover for gesture control."""

#
#
# Warning: This is just an example how to use hover and pygame
# to control some playing musics
# There's no error checking if you get to the last track and try to play a next one
# or if you get back and try to play -1 track
#
#

__program__   = "playerHover.py"
__author__    = "Bruno Santos (feiticeir0)"
__copyright__ = "Public domain"

import pygame
import array
import time
import os
import glob
import warnings
import random
from Hover_library import Hover

hover = Hover(address=0x42, ts=23, reset=24)

# function to play given track
def playTrack (trackNum):
	# clear screen
	os.system('clear');
	pygame.mixer.music.load(playFiles[trackNum])
	pygame.mixer.music.play()
	filepath,filename = os.path.split(playFiles[trackNum])
	print ("Playing track ",trackNum, filename)
    #while pygame.mixer.music.get_busy():
     #   time.sleep(1)


# music files location - Change only here
toPath = '<mp3 files location - full path>'

# Declare array for mp3 files
playFiles = []

# Get the files
for root, dirs, files in os.walk(toPath):
    playFiles += glob.glob(os.path.join(root, '*.mp3'))

# Just for info on order
#for f in playFiles:
#    print (f)

#get size of array
numOfTracks = len (playFiles)
#print ("Number of tracks: ",numOfTracks)


#Start at random track
#sTrack = random.randrange(0,(numOfTracks-1))
#print ("Starting at",sTrack," : ", playFiles[sTrack])

sTrack = 0

#fill played track
#playedFiles[sTrack] = 1

# Initialize the mixer
#pygame.mixer.init(48000, -16, 1, 1024)
pygame.mixer.init()

# start playing
playTrack (sTrack)

# hover stuff
try:
	while True:
		# Verify hover
		if (hover.getStatus() == 0):
			# Read i2c data
			message = hover.getEvent()
			type (message)
			if (message == "00100100"):
				sTrack -= 1
				if (sTrack <= 0):
					print ("No previous track!")
					sTrack = 0
				else:
					playTrack (sTrack)

			if (message == "00100010"):
				sTrack += 1
				if (sTrack >= (numOfTracks)):
					print ("No more tracks to play!")
					sTrack = (numOfTracks - 1)
				else:
					playTrack (sTrack)

			if (message == "00101000"):
				print ("Pause")
				pygame.mixer.music.pause()
			if (message == "00110000"):
				print ("Unpause")
				pygame.mixer.unpause()
			if (message == "01010000"):
				print ("Play");
				pygame.mixer.music.play()
			if (message == "01000001"):
				print ("Stop")
				pygame.mixer.music.stop()
			if (message == "01000010"):
				print ("Vol--")
				pygame.mixer.music.set_volume(pygame.mixer.music.get_volume() - 0.1)
			if (message == "01001000"):
				print ("Vol++");
				pygame.mixer.music.set_volume(pygame.mixer.music.get_volume() + 0.1)
			hover.setRelease()
		time.sleep(0.0008)

except KeyboardInterrupt:
	print ("Exiting..")
	hover.end()
except:
	print ("Something went wrong")
	hover.end()

