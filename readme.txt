This is a simple project to show how to use hover to control some mp3 files playing

How to execute the project:

sudo python playerHover.py

Gestures:
	. Hover left - previous track
	. Hover right - next track
	. Hover top - pause
	. Hover down - unpause
	. touch center - play
	. touch down - stop
	. touch left - vol down
	. touch right - vol up 


The following files belong to Hover. I'm just including them here because they are needed. 
In your project, copy the newer ones (probably in the master.zip file) to the directory of playerHover.py

Hover_library.py
Hover_library.pyc
